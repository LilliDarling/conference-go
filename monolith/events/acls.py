from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json



def get_photo(city, state):
    headers = {
        'Authorization': PEXELS_API_KEY
    }
    params = {
        "per_page": 1,
        "query": city + " "+ state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    
    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.text)

    if not content or len(content) == 0:
        return {"error": "No geocoding data found"}
    
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=metric&appid={OPEN_WEATHER_API_KEY}" 
    response = requests.get(url)
    content = json.loads(response.text)

    if 'main' not in content or 'weather' not in content:
        return {"error": "Weather data not found"}
    
    temp = content["main"]["temp"]
    desc = content["weather"][0]["description"]

    return {
        "temperature": temp,
        "description": desc
    }

